# Installation

## thorin

```bash
# Boot into NixOS GNOME ISO

# Generate a LUKS keyfile for the encrypted partition
dd bs=512 count=4 if=/dev/urandom of=/tmp/luks-crypt.key iflag=fullblock
chmod 400 /tmp/luks-crypt.key

# Format and mount the disks
disko -m destroy,format,mount --flake .#thorin --argstr withRootOS alpine

# Bootstrap Alpine into /mnt
apk --arch x86_64 -X http://dl-cdn.alpinelinux.org/alpine/edge/main \
  -U --allow-untrusted --root /mnt --initdb add \
  alpine-base linux-lts linux-firmware-other bash ansible sops git

# Move the generated LUKS keyfile into the chroot
mv /tmp/luks-crypt.key /mnt/root/

# Generate /etc/fstab (use genfstab script from arch-install-scripts package)
genfstab -U /mnt >> /mnt/etc/fstab

# Prepare for the chroot
cp /etc/resolv.conf /mnt/etc/resolv.conf
arch-chroot /mnt/
export PATH=/bin:/sbin:/usr/bin:/usr/sbin

# Change root password
passwd
# or lock the root account
passwd -l root

# Update initramfs
echo amdgpu >> /etc/modules
echo fbcon >> /etc/modules
cat <<-EOF > /etc/mkinitfs/mkinitfs.conf
features="ata base btrfs xfs keymap kms mmc nvme scsi usb virtio cryptsetup"
initfscomp="zstd"
EOF
apk fix mkinitfs

# Install limine bootloader
apk add limine limine-x86_64 limine-efi-updater
cat <<-EOF >/etc/limine/limine-efi.conf
efi_system_partition=/efi
efi_file="BOOTX64.EFI"
# disable_update_hook=1
EOF
cat <<-EOF > /boot/limine.conf
timeout: 10

/Alpine Linux (LTS)
    protocol: linux
    kernel_path: boot():/vmlinuz-lts
    kernel_cmdline: root=UUID=<uuid_of_crypted-root_btrfs_partition> ro quiet modules=sd-mod,usb-storage,btrfs,nvme,amdgpu rootflags=defaults,noatime,compress=zstd:9,subvol=@root/alpine rootfstype=btrfs cryptroot=UUID=<uuid_of_encrypted_luks_partition> cryptdm=crypt-root apparmor=1 security=apparmor
    module_path: boot():/amd-ucode.img
    module_path: boot():/initramfs-lts
EOF
apk fix limine-efi-updater

# Execute Ansible system roles inside the chroot
ANSIBLE_SOPS_AGE_KEYFILE=/tmp/age-private.txt ansible-playbook playbooks/system.yml --limit thorin

# Exit the chroot and reboot
exit
umount -R /mnt
cryptsetup close crypt-root
cryptsetup close crypt-media
reboot
```
