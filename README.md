# dotfiles v2

This is the continuation of [my old dotfiles](/folliehiyuki/dotfiles) without all the Xorg stuff.

## Usage

> [!NOTE]
> All hosts use the same connection method `local`. Hence, each ansible-playbook
> command needs to specify exactly 1 hostname via `--limit` (or else the
> playbook will be run multiple times).

```bash
# system.yml should be the first to run. It executes all tasks as the `root` user.
ansible-playbook playbooks/system.yml --limit '<host_name>'

# local.user.main playbook configures $HOME directory
ansible-playbook local.user.main --limit '<host_name>'
```

**home-manager** configuration is also provided for hosts using Nix. To configure:

```bash
# Ensure the managed user is a `trusted-user` in /etc/nix/nix.conf
# so some Nix configuration can be set by home-manager

# Source $XDG_STATE_HOME/nix/profile/etc/profile.d/hm-session-vars.sh in your shell config
# (it'll be $HOME/.nix/profile/etc/profile.d/hm-session-vars.sh if use-xdg-base-directories=false in nix.conf)

nix run .#home-manager -- switch --flake .#<host>/<username>
```

## 🌟 Credits

- [eoli3n/dotfiles](https://github.com/eoli3n/dotfiles)
- [samhh/dotfiles](https://github.com/samhh/dotfiles)
- [zsugabubus/dotfiles](https://github.com/zsugabubus/dotfiles)
- [sircmpwn/dotfiles](https://git.sr.ht/~sircmpwn/dotfiles)
- [rjarry/dotfiles](https://git.sr.ht/~rjarry/dotfiles)
- [wangl-cc/dotfiles](https://github.com/wangl-cc/dotfiles)

## 📄 License

MIT
