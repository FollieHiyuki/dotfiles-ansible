{
  description = "folliehiyuki's dotfiles using Ansible";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    git-hooks = {
      url = "github:cachix/git-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
  };

  outputs =
    inputs@{ flake-parts, ... }:
    let
      inherit (inputs.nixpkgs) lib;
    in
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = [
        "x86_64-linux"
        "aarch64-linux"
      ];

      imports = [ ./nix/devShell.nix ];

      flake = with lib; {
        diskoConfigurations =
          let
            diskConfigs = filterAttrs (name: type: type == "regular" && hasSuffix ".nix" name) (
              builtins.readDir ./nix/disko
            );
          in
          mapAttrs' (
            name: _: nameValuePair (removeSuffix ".nix" name) (import ./nix/disko/${name})
          ) diskConfigs;

        homeConfigurations =
          let
            hosts = {
              thorin = "x86_64-linux";
            };

            registryInputs = mapAttrs (_: val: { flake = val; }) (
              filterAttrs (
                name: value: name != "self" && (builtins.hasAttr "_type" value) && value._type == "flake"
              ) inputs
            );
          in
          concatMapAttrs (host: value: mapAttrs' (user: val: nameValuePair "${host}/${user}" val) value) (
            mapAttrs (
              host: system:
              mapAttrs'
                (
                  filename: _:
                  let
                    username = removeSuffix ".nix" filename;

                    pkgs = import inputs.nixpkgs {
                      inherit system;
                      config.allowUnfree = true;
                    };
                  in
                  nameValuePair username (
                    inputs.home-manager.lib.homeManagerConfiguration {
                      inherit pkgs;

                      extraSpecialArgs = {
                        inherit inputs;
                      };

                      modules = [
                        inputs.nix-index-database.hmModules.nix-index
                        ./nix/home-manager/${host}/${filename}
                        {
                          home = {
                            inherit username;
                            stateVersion = "24.11";
                          };

                          programs.home-manager.enable = true;

                          nixpkgs.config.allowUnfree = true;

                          nix = {
                            nixPath = [
                              "nixpkgs=${inputs.nixpkgs}"
                              "home-manager=${inputs.home-manager}"
                              "/nix/var/nix/profiles/per-user/root/channels"
                            ];

                            registry = registryInputs // {
                              dotfiles.flake = inputs.self;
                            };

                            package = pkgs.nixVersions.latest;
                          };
                        }
                      ];
                    }
                  )
                )
                (
                  filterAttrs (name: type: type == "regular" && hasSuffix ".nix" name) (
                    builtins.readDir ./nix/home-manager/${host}
                  )
                )
            ) hosts
          );
      };
    };
}
