blacklisted_kernel_modules:
  - btusb
  - bluetooth

qemu_system_packages:
  - qemu-system-arm
  - qemu-system-aarch64
  - qemu-system-x86_64

tlp_config: |
  TLP_DEFAULT_MODE=BAT
  CPU_SCALING_MIN_FREQ_ON_BAT=1400000
  CPU_SCALING_MAX_FREQ_ON_BAT=1700000
  CPU_SCALING_MIN_FREQ_ON_AC=1400000
  CPU_SCALING_MAX_FREQ_ON_AC=2100000
  DEVICES_TO_DISABLE_ON_STARTUP="bluetooth nfc wifi wwan"
  DEVICES_TO_DISABLE_ON_BAT_NOT_IN_USE="bluetooth nfc wifi wwan"
  DISK_IOSCHED="mq-deadline mq-deadline"
  START_CHARGE_THRESH_BAT0=75
  STOP_CHARGE_THRESH_BAT0=90
  RESTORE_THRESHOLDS_ON_BAT=1

snapshot_tool: btrbk

btrbk_options:
  lockfile: /var/lock/btrbk.lock
  cache_dir: /var/cache/btrbk
  compat: busybox
  transaction_syslog: cron
  timestamp_format: long
  backend_local_user: btrfs-progs-doas
  snapshot_create: onchange
  snapshot_preserve: 16h 8d 4w 2m
  snapshot_preserve_min: 6h
  snapshot_dir: /.snapshots

btrbk_sections:
  - type: subvolume
    path: /
  - type: subvolume
    path: /home/follie
  - type: subvolume
    path: /media
    options:
      snapshot_dir: /media/.snapshots
      snapshot_preserve_min: latest

dns_resolver: dnscrypt-proxy

dnscrypt_lb_strategy: random

dnscrypt_log_file: /var/log/dnscrypt-proxy/dnscrypt-proxy.log

dnscrypt_query_log_file: /var/log/dnscrypt-proxy/query.log

dnscrypt_nx_log_file: /var/log/dnscrypt-proxy/nx.log

dnscrypt_blocked_ips_log_file: /var/log/dnscrypt-proxy/blocked-ips.log

nix_extra_config:
  auto-allocate-uids: 'true'
  connect-timeout: 5
  experimental-features: cgroups auto-allocate-uids ca-derivations flakes nix-command
  http-connections: 0
  keep-outputs: 'true'
  keep-derivations: 'true'
  log-lines: 25
  max-free: 3221225472 # 3GB
  min-free: 536870912 # 512MB
  trusted-users: root @wheel
  use-cgroups: 'true'
  warn-dirty: 'false'

greetd_session_command: >-
  /usr/bin/tuigreet
  --asterisks
  --issue
  --time --time-format '%a, %d %b %Y - %H:%M:%S'
  --power-shutdown 'loginctl poweroff'
  --power-reboot 'loginctl reboot'
  --kb-command 2
  --kb-sessions 3
  --kb-power 12

# tuigreet automatically sets up XDG_SESSION_DESKTOP, XDG_SESSION_TYPE and
# XDG_CURRENT_DESKTOP for us, so no need to specify them here
pam_global_envs:
  GDK_BACKEND: wayland
  CLUTTER_BACKEND: gdk # https://gitlab.gnome.org/GNOME/clutter/-/merge_requests/5
  QT_QPA_PLATFORM: wayland-egl
  # QT_WAYLAND_FORCE_DPI: physical
  QT_WAYLAND_DISABLE_WINDOWDECORATION: 1
  ELM_DISPLAY: wl
  ECORE_EVAS_ENGINE: wayland_egl
  ELM_ENGINE: wayland_egl
  ELM_ACCEL: opengl
  # ELM_SCALE: 1
  SDL_VIDEODRIVER: wayland
  _JAVA_AWT_WM_NONREPARENTING: 1
  MOZ_ENABLE_WAYLAND: 1
  MOZ_DBUS_REMOTE: 1
  NO_AT_BRIDGE: 1 # https://bbs.archlinux.org/viewtopic.php?id=189975
  WINIT_UNIX_BACKEND: wayland
  XKB_DEFAULT_LAYOUT: us
  GTK_IM_MODULE: fcitx
  QT_IM_MODULE: fcitx
  SDL_IM_MODULE: fcitx
  XMODIFIERS: '@im=fcitx'

fstab_restrict_proc: true

users__system:
  follie:
    shell: /usr/bin/fish
    comment: Kawaii Linux user
    groups:
      - audio
      - libvirt
      - pipewire
      - polkitd
      - users
      - video
      - wheel

subgid_entries: &subgid_conf
  - user: root
    subid: 1000000
    count: 1000000000
  - user: follie
    subid: 100000
    count: 65536

subuid_entries: *subgid_conf
