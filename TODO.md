# TODO

Plans for the future of this dotfiles.

## General

- [ ] ~Add [GuixWL](https://guixwl) workflow~
- [ ] Add testing environments and assert scripts with either:
  - virt-lightning
  - virter
  - opentofu / pulumi
  - ansible's molecule

## System

### Configuration

- [ ] Spawn superd/openrc/dinit process at user level with system's service manager or pam_exec instead of inside compositor's config file
  - Look at the new openrc's /etc/init.d/user service template
  - Enable in the `users` role of alpine collection
  - Reuse `user_service_manager` variable from user collection
  - Remove `start_user_services_with_compositor` variable
  - Ref: <https://wiki.gentoo.org/wiki/OpenRC/User_services>
- [ ] /etc/security/{access,limits}.conf (PAM module configuration)
- [ ] Filesystem snapshot:
  - [ ] zrepl (rootfs=zfs)
- [ ] Root on tmpfs / immutable root
  - https://wiki.alpinelinux.org/wiki/Immutable_root_with_atomic_upgrades
  - https://wiki.alpinelinux.org/wiki/Lbu
- [ ] incron
- [ ] Bring back GRUB role
  - [ ] Configure kernel boot parameters (https://wiki.archlinux.org/title/Kernel_parameters)
- [ ] bees
- [ ] `kea` as another option for dhcp client
- [ ] booster and dracut options for initramfs_generator
- [ ] `i915.enable_guc=3` (/etc/modprobe.d/kms.conf)
- [ ] Add a role to configure different distro chroots (<https://wiki.alpinelinux.org/wiki/Running_glibc_programs#Chroot>)
- [ ] swapfile + hibernation setup
  - [ ] `resume=` with swapfile offset kernel boot parameter
  - [ ] btrfs filesystem mkswapfile --size 4g --uuid clear /swap/swapfile in dedicated subvolume `@swap`
- [ ] zswap as an alternative to zram (with swapfile set up beforehand)

### Just in case I forget

- [ ] ~nftables with rootful podman (<https://github.com/greenpau/cni-plugins>)~
- [ ] Write docs about AlpineLinux installation:
  - [ ] Bootloader configuration:
    - [ ] limine
      - Only manage limine.conf
      - enroll-config with auto-computed b2sum + inject b2sum to path in the config file
    - [ ] ukify / stubbyboot
    - [ ] Configure kernel-hooks or incron package in the case of stubbyboot, ukify and limine
    - [ ] EFI secure boot (also sign fwupd and shell efi binaries)
      - Specify options (ukify or separated initramfs/kernel files) => allow choosing targets to sbsign
  - [ ] Common kernel parameters: `init_on_free=1 page_alloc.shuffle=1 lockdown=integrity quiet`
    - [ ] ZFS: `root=ZFS=rpool/ROOT/alpine`
    - [ ] BTRFS: `modules=sd-mod,usb-storage,btrfs,nvme rootfstype=btrfs cryptroot=UUID=<...> cryptdm=alpine`

## Dotfiles

### Software

- [ ] ~~[wayout](https://git.sr.ht/~proycon/wayout)~~
- [x] ~~Use [bubblewrap](https://github.com/containers/bubblewrap) for some applications~~ ==> replace with rootless [podman](https://podman.io) and [apptainer](https://apptainer.org)
- [ ] [eww](https://github.com/elkowar/eww)
- [ ] ~~[swhkd](https://github.com/waycrate/swhkd)~~
- [ ] [senpai](https://git.sr.ht/~taiite/senpai) / simpler weechat config
- [ ] [ripgrep](https://github.com/BurntSushi/ripgrep) -> [ugrep](https://github.com/Genivia/ugrep)
- [ ] [vieb](https://github.com/Jelmerro/Vieb)
- [ ] ~~[wolfssh](https://www.wolfssl.com)~~
- [ ] [AutoSub](https://github.com/abhirooptalasila/AutoSub)
- [ ] ~~[fnm](https://github.com/Schniz/fnm)~~
- [ ] ~~[asdf](https://github.com/asdf-vm/asdf)~~
- [ ] [userspace-tablet-driver](https://github.com/kurikaesu/userspace-tablet-driver-daemon) for my XP-PEN Artist 16 Pro
- [ ] [lite-xl](https://github.com/lite-xl/lite-xl) / helix / kakoune
- [ ] Wayland compositors: [niri](https://github.com/YaLTeR/niri), [japokwm](https://github.com/werererer/japokwm), [dwl](https://github.com/djpohly/dwl), [labwc](https://github.com/labwc/labwc), [vivarium](https://github.com/inclement/vivarium), [qtile](https://github.com/qtile/qtile) (also reevaluate [hikari](https://hub.darcs.net/raichoo/hikari) and [wayfire](https://wayfire.org))
- [x] ~~[tremc](https://github.com/tremc/tremc) / [rtorrent](https://github.com/rakshasa/rtorrent/)~~ ==> I use [qbt](https://github.com/ludviglundgren/qbittorrent-cli) with qbittorrent-nox daemon now
- [ ] Other terminals (contour, wezterm)
- [ ] CopyQ (config, themes + superd service + clipboard menu script)
- [ ] mangal
- [ ] musikcube
- [ ] vimiv-qt
- [ ] mpd-mpris (as an alternative to mpDris2)
- [ ] chromium policies (https://www.chromium.org/administrators/linux-quick-start/)

### Cosmetic

- [ ] GTK/Icons/Cursor theme, Sarasa font bootstrapping
- [ ] Waybar/i3status-rust config file for River
- [ ] [catppuccin](https://github.com/catppuccin/catppuccin) theme
- [ ] Remove `pipewire-pulse` entirely (is there alternative to pavucontrol for pipewire??)
- [ ] ~~qBittorrent themes~~
- [ ] waybar: pulseaudio module -> wireplumber module (wait for more features, e.g. scrolling)
- [ ] html2text, html2org
- [ ] Build podman container images with ansible-bender or stacker
- [ ] Add `target-determinator` and `aspect-cli` to packages (or containers, with underlying `bazel`)

### Sandboxing

- [ ] podman rootless with dbus access (without ANONYMOUS authentication) + socket permissions (wayland / dbus/ pipewire / pulseaudio) in container (777 is no good)
