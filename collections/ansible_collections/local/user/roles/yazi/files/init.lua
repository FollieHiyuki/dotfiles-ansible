-- Show username and hostname in the header line
Header:children_add(function()
  if ya.target_family() ~= 'unix' then
    return ''
  end
  return ui.Span(ya.user_name() .. '@' .. ya.host_name() .. ':'):fg(th.mode.normal_main.bg)
end, 500, Header.LEFT)

-- Show user/group of the hovered file entry in status line
Status:children_add(function(self)
  local h = self._current.hovered
  if h == nil or ya.target_family() ~= 'unix' then
    return ''
  end

  return ui.Line {
    ui.Span(ya.user_name(h.cha.uid) or tostring(h.cha.uid)):fg(th.mgr.find_position.fg),
    ':',
    ui.Span(ya.group_name(h.cha.gid) or tostring(h.cha.gid)):fg(th.mgr.find_position.fg),
    ' ',
  }
end, 500, Status.RIGHT)

-- Display symlink in status line
Status:children_add(function(self)
  local h = self._current.hovered
  if h and h.link_to then
    return ' -> ' .. tostring(h.link_to)
  else
    return ''
  end
end, 3300, Status.LEFT)

-- Display the full mode name
function Status:mode()
  local mode = tostring(self._tab.mode):upper()

  local style = self:style()
  return ui.Line {
    ui.Span(th.status.sep_left.open):fg(style.main.bg):bg('reset'),
    ui.Span(' ' .. mode .. ' '):style(style.main),
    ui.Span(th.status.sep_left.close):fg(style.main.bg):bg(style.alt.bg),
  }
end

require('full-border'):setup {
  type = ui.Border.ROUNDED,
}

require('smart-enter'):setup {
  open_multi = true,
}
