#!/usr/bin/env bash

DIRECTORY="/run/media/$USER"

if [ -d "$DIRECTORY" ]; then
	dir_list=$(find "$DIRECTORY" -type d -maxdepth 1 | tail -n +2)
	if [ -n "$dir_list" ]; then
		# Loop through everything, in case we unmounted manually (not using udisks) and left stale directories behind
		final_result=0
		for dir in ${dir_list}; do
			if mountpoint -q "$dir"; then
				final_result=$(( 1 || final_result ))
			fi
		done

		[ "$final_result" -eq 1 ] && echo "󱊞 "
		exit
	fi
fi
