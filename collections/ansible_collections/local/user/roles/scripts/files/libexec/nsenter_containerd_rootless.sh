#!/bin/sh

pid=$(cat "${XDG_RUNTIME_DIR:-/run/user/$(id -u)}/containerd-rootless/child_pid")
exec nsenter \
	--no-fork \
	--wd="$HOME" \
	--preserve-credentials \
	--mount \
	--net \
	--user \
	--target "$pid" \
	-- "$@"
