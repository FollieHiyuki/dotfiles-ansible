- name: Create SIF container image for bazel
  tags: bazel
  vars:
    sif_location: '{{ xdg_dir.libexec_dir }}/bazel.sif'
  block:
    - name: Clean old bazel.sif file
      ansible.builtin.command:
        cmd: rm -f {{ sif_location }}
        removes: '{{ sif_location }}'

    - name: Pull and build bazel.sif file
      ansible.builtin.command:
        cmd: apptainer pull {{ sif_location }} docker://gcr.io/bazel-public/bazel:{{ bazel_version }}
        creates: '{{ sif_location }}'

    - name: Create local bazel wrapper script
      ansible.builtin.copy:
        content: |
          #!/bin/sh
          TERM=xterm-256color exec apptainer --silent --quiet exec \
            --ipc --no-eval --no-privs --unsquash --userns --uts \
            {{ sif_location }} bazel "$@"
        dest: '{{ xdg_dir.bin_home }}/bazel'
        mode: '755'

- name: Pull and use vagrant-libvirt container image
  tags: vagrant
  vars:
    vagrant_image_repository: docker.io/vagrantlibvirt/vagrant-libvirt
    vagrant_image_tag: latest-slim
  block:
    - name: Update vagrant-libvirt image
      containers.podman.podman_image:
        name: '{{ vagrant_image_repository }}'
        tag: '{{ vagrant_image_tag }}'
        force: true

    - name: Create vagrant wrapper script
      ansible.builtin.template:
        src: vagrant.j2
        dest: '{{ xdg_dir.bin_home }}/vagrant'
        mode: '755'

- name: Create wrapper scripts for kcli
  tags: kcli
  block:
    - name: Create kcli wrapper script
      ansible.builtin.template:
        src: kcli.j2
        dest: '{{ xdg_dir.bin_home }}/kcli'
        mode: '755'

    - name: Symlink kweb script to kcli
      ansible.builtin.file:
        src: kcli
        dest: '{{ xdg_dir.bin_home }}/kweb'
        state: link

- name: Build aports-dev container image and set up wrapper scripts
  tags: aports-dev
  vars:
    aports_dev_version: '{{ now(fmt="%Y-%m-%d") }}'
    builder_uid: '1069'
  block:
    - name: Build aports-dev container image
      containers.podman.podman_image:
        name: aports-dev
        path: '{{ role_path }}/build_dirs/aports-dev'
        tag: '{{ aports_dev_version }}'
        build:
          extra_args: >-
            --build-arg BUILD_USER_ID={{ builder_uid }}
            --build-arg BUILD_DATE={{ aports_dev_version }}
        state: present
      register: aports_dev_image

    - name: Create necessary bind-mount directories for aports-dev container to run
      ansible.builtin.file:
        path: '{{ item }}'
        state: directory
        mode: '755'
      loop:
        - '{{ ansible_facts["user_dir"] }}/.abuild'
        - '{{ abuild_packages_path }}'

    - name: Check whether an abuild key pair has been generated
      ansible.builtin.find:
        paths:
          - '{{ ansible_facts["user_dir"] }}/.abuild'
        file_type: file
        recurse: false
        patterns:
          - '*.rsa'
      register: abuild_key_found

    - name: Run abuild-keygen
      containers.podman.podman_container:
        name: abuild-keygen
        image: '{{ aports_dev_image.image[0].RepoDigests[0] }}'
        entrypoint: ''
        rm: true
        command:
          - /usr/bin/abuild-keygen -a -n
        user: '{{ builder_uid }}'
        gidmap:
          - '{{ builder_uid }}:0:1'
          - '0:1:{{ builder_uid }}'
        uidmap:
          - '{{ builder_uid }}:0:1'
          - '0:1:{{ builder_uid }}'
        volume:
          - '{{ ansible_facts["user_dir"] }}/.abuild:/home/builder/.abuild'
          - '{{ xdg_dir.config_home }}/git/config:/home/builder/.gitconfig'
      when: abuild_key_found.matched == 0

    - name: Copy aports-dev and abuild wrapper script
      vars:
        podman_args: >-
          --rm
          --tty
          --interactive
          --gidmap {{ builder_uid }}:0:1
          --gidmap 0:1:{{ builder_uid }}
          --uidmap {{ builder_uid }}:0:1
          --uidmap 0:1:{{ builder_uid }}
          --volume abuild-distfiles:/var/cache/distfiles
          --volume abuild-apkcache:/var/cache/apk
          --volume abuild-ccache:/home/builder/.ccache
          --volume {{ ansible_facts["user_dir"] }}/.abuild:/home/builder/.abuild
          --volume {{ abuild_packages_path }}:/home/builder/packages
          --volume {{ aports_local_path }}:/home/builder/aports
        container_image_repo: '{{ aports_dev_image.image[0].RepoDigests[0] }}'
      ansible.builtin.template:
        src: '{{ item }}.j2'
        dest: '{{ xdg_dir.bin_home }}/{{ item }}'
        mode: '755'
      loop:
        - aports-dev
        - abuild
