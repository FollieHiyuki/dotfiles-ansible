#!/bin/sh

# copy abuild's public keys to trusted store
doas cp "$HOME"/.abuild/*.rsa.pub /etc/apk/keys/

# make sure distfiles has correct permissions
doas install -d -m 775 -g abuild /var/cache/distfiles

# correct permissions of podman's volumes
for vpath in "$HOME"/.ccache "$HOME"/.abuild "$HOME"/packages; do
	[ -d "$vpath" ] && doas chown builder:builder "$vpath"
done

exec "$@"
