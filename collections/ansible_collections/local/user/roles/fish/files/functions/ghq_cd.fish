function ghq_cd
  set -l proj (ghq list --full-path | fzf \
    --no-multi \
    --prompt "Project: " \
    --preview 'lsd -1FAL --group-dirs first --icon always --color always {}' \
    )
  if test -n "$proj"
    cd -- "$proj"
  end
end
