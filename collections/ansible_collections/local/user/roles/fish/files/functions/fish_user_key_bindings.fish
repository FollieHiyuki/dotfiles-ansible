function fish_user_key_bindings
  # Use fzf for <Ctrl+R>, <Ctrl+T>, <Alt+C>
  status is-interactive; and fzf_key_bindings

  # Equal to <Alt+↓> and <Alt+↑>
  bind -M insert \ej history-token-search-forward
  bind -M insert \ek history-token-search-backward
end
