function ghq_rm
  set -l projects (ghq list --exact | fzf --multi --prompt "Project: ")

  if test -n "$projects"
    for proj in $projects
      set -l proj_dir (ghq list --full-path "$proj")
      ghq rm "$proj"; and begin
        rmdir -p (dirname "$proj_dir") 2>/dev/null; or true
      end
    end
  end
end
