- name: Create config directories
  ansible.builtin.file:
    path: '{{ xdg_dir.config_home }}/fish/{{ item }}'
    state: directory
    mode: '755'
  loop:
    - completions
    - conf.d
    - functions
    - themes

- name: Copy functions
  ansible.builtin.copy:
    src: 'functions/{{ item }}'
    dest: '{{ xdg_dir.config_home }}/fish/functions/{{ item }}'
    mode: '644'
  loop:
    - fish_greeting.fish
    - fish_user_key_bindings.fish
    - ghq_cd.fish
    - ghq_rm.fish

- name: Copy config templates
  ansible.builtin.template:
    src: '{{ item }}.j2'
    dest: '{{ xdg_dir.config_home }}/fish/{{ item }}.fish'
    mode: '644'
  loop:
    - conf.d/aliases
    - conf.d/env
    - config

- name: Copy fish theme files
  ansible.builtin.copy:
    src: '{{ item }}'
    dest: '{{ xdg_dir.config_home }}/fish/themes/{{ item }}'
    mode: '644'
  loop:
    - Onedark.theme
    - Nord.theme

- name: Check whether fish shell is installed
  ansible.builtin.command: which fish
  ignore_errors: true
  changed_when: false
  register: fish_installed

# Overwrite theme? [y/N]
- name: Set fish theme to `{{ theme }}`
  ansible.builtin.shell:
    cmd: fish_config theme save {{ theme | capitalize }}
    executable: '{{ fish_installed.stdout }}'
    stdin: y
    stdin_add_newline: true
  changed_when: true
  when: fish_installed.rc == 0

- name: Download fzf_key_bindings.fish
  ansible.builtin.get_url:
    url: https://github.com/junegunn/fzf/raw/master/shell/key-bindings.fish
    dest: '{{ xdg_dir.config_home }}/fish/functions/fzf_key_bindings.fish'
    mode: '644'

- name: Download plugin-foreign-env plugin
  ansible.builtin.get_url:
    url: https://github.com/oh-my-fish/plugin-foreign-env/raw/master/functions/{{ item }}
    dest: '{{ xdg_dir.config_home }}/fish/functions/{{ item }}'
    mode: '644'
  loop:
    - fenv.fish
    - fenv.main.fish

- name: Download autopair.fish plugin
  ansible.builtin.get_url:
    url: https://github.com/jorgebucaran/autopair.fish/raw/main/{{ item }}
    dest: '{{ xdg_dir.config_home }}/fish/{{ item }}'
    mode: '644'
  loop:
    - conf.d/autopair.fish
    - functions/_autopair_backspace.fish
    - functions/_autopair_insert_left.fish
    - functions/_autopair_insert_right.fish
    - functions/_autopair_insert_same.fish
    - functions/_autopair_tab.fish
