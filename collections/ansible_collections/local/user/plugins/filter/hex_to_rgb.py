from ansible.errors import AnsibleFilterError, AnsibleFilterTypeError
from ansible.module_utils._text import to_native
from ansible.module_utils.six import string_types


def hex_to_rgb(color: str):
    """
    Convert a HEX color string to a list of RGB values.

    Example: '#2e3440' -> [46, 52, 64].
    """
    if isinstance(color, string_types):
        if len(color) == 7 and color[0] == "#":
            try:
                red = int(color[1:3], base=16)
                green = int(color[3:5], base=16)
                blue = int(color[5:7], base=16)

                return [red, green, blue]
            except Exception as e:
                raise AnsibleFilterError(
                    f"hex_to_rgb - {to_native(e)}", orig_exc=e
                ) from e
        else:
            raise AnsibleFilterError(
                f"hex_to_rgb expects 7-character string started with '#', got {color} instead."
            )
    else:
        raise AnsibleFilterTypeError(
            f"|hex_to_rgb expects string, got {type(color)} instead."
        )


class FilterModule(object):
    """Custom Ansible jinja2 filters for dotfiles playbook."""

    def filters(self):
        return {"hex_to_rgb": hex_to_rgb}
