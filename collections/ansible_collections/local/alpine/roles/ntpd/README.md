# Notes

Tasks not needed in NTP client mode but might come handy in the future.

## Generate NTP symetric key for chronyd

**NOTE**: NTS should be used if the server is publicly accessible. Otherwise, for a private NTP server, symetric key is sufficient.

```yaml
- name: chrony | Generate the symetric key with chronyc
  command:
    cmd: /usr/bin/chronyc keygen {{ (2**32 - 1) | random }} SHA512 256
    creates: /etc/chrony/chrony.keys
  register: chrony_keys

- name: chrony | Create chrony.keys file from chronyc output
  copy:
    content: |
      {{ chrony_keys['stdout'] }}
    dest: /etc/chrony/chrony.keys
    mode: '400'
    owner: chrony
    group: root
  when: chrony_keys is defined
```

Then add `keyfile /etc/chrony/chrony.keys` to ***/etc/chrony/chrony.conf*** file.
