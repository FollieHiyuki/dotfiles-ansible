# Luckily, disko script automatically injects `lib` as a function argument for us, so no need to
# deal with merging impure arguments in CLI with hardcoded { inherit lib; } inside the `import` call.
#
# Ref: https://github.com/nix-community/disko/blob/856a2902156ba304efebd4c1096dbf7465569454/cli.nix#L54

{
  lib,
  withRootOS ? "alpine",
  ...
}:
let
  btrfsArgs = [
    "--force"
    "--checksum blake2" # more "secure" than the default crc32c, still relatively fast
    "--features block-group-tree" # NOTE: put here until it's the default
  ];

  btrfsMountOptions = [
    "noatime"
    "compress=zstd:9"
  ];

  stricterMountOptions = btrfsMountOptions ++ [
    "nodev"
    "noexec"
    "nosuid"
  ];
in
{
  disko.devices.disk = {
    nvme0n1 = {
      device = "/dev/disk/by-id/nvme-eui.ace42e0095951092";
      type = "disk";
      content = {
        type = "gpt";
        partitions = {
          ESP = {
            size = "4G";
            type = "EF00";
            content = {
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
              mountOptions = [ "umask=0077" ];
            };
          };
          luks = {
            size = "100%";
            content = {
              type = "luks";
              name = "crypt-root";
              askPassword = true;
              content = {
                type = "btrfs";
                extraArgs = btrfsArgs;
                subvolumes =
                  with lib;
                  mergeAttrsList [
                    (genAttrs
                      [
                        "@root/alpine"
                        "@root/chimera"
                      ]
                      (
                        subvol:
                        optionalAttrs (withRootOS == (last (splitString "/" subvol))) {
                          mountpoint = "/";
                          mountOptions = btrfsMountOptions;
                        }
                      )
                    )

                    # Both AlpineLinux and ChimeraLinux use apk package manager
                    (genAttrs [ "@var/cache/apk_alpine" "@var/cache/apk_chimera" ] (
                      subvol:
                      optionalAttrs (withRootOS == (last (splitString "_" subvol))) {
                        mountpoint = "/var/cache/apk";
                        mountOptions = stricterMountOptions;
                      }
                    ))

                    (genAttrs
                      [
                        "@var/cache"
                        "@var/guix"
                        "@var/lib"
                        "@var/log"
                        "@var/tmp"
                      ]
                      (subvol: {
                        mountpoint = builtins.replaceStrings [ "@" ] [ "/" ] subvol;
                        mountOptions = stricterMountOptions;
                      })
                    )

                    {
                      "@root" = { };
                      "@home" = { };
                      "@home/follie" = {
                        mountpoint = "/home/follie";
                        mountOptions = btrfsMountOptions;
                      };
                      "@gnu" = {
                        mountpoint = "/gnu";
                        mountOptions = btrfsMountOptions ++ [ "nodev" ];
                      };
                      "@nix" = {
                        mountpoint = "/nix";
                        mountOptions = btrfsMountOptions ++ [ "nodev" ];
                      };
                      "@var" = { };
                      "@snapshots" = {
                        mountpoint = "/.snapshots";
                        mountOptions = btrfsMountOptions;
                      };
                    }
                  ];
              };
            };
          };
        };
      };
    };

    sda = {
      device = "/dev/disk/by-id/wwn-0x5000039a5280546c";
      type = "disk";
      content = {
        type = "gpt";
        partitions.luks = {
          size = "100%";
          content = {
            type = "luks";
            name = "crypt-media";
            askPassword = true;
            additionalKeyFiles = [ "/tmp/luks-crypt.key" ];
            content = {
              type = "btrfs";
              extraArgs = btrfsArgs;
              subvolumes = {
                "@data" = {
                  mountpoint = "/media";
                  mountOptions = stricterMountOptions;
                };
                "@snapshots" = {
                  mountpoint = "/media/.snapshots";
                  mountOptions = stricterMountOptions;
                };
              };
            };
          };
        };
      };
    };
  };
}
