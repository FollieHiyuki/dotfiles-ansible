{ config, pkgs, ... }:
{
  home.homeDirectory = "/home/${config.home.username}";

  home.packages = with pkgs; [
    deno
    dune_3
    git-privacy
    ltex-ls
    manix
    marksman
    mill
    nixfmt-rfc-style
    nurl
    ocamlPackages.ocaml-lsp
    ocamlPackages.ocamlformat
    opam
    purescript
    sbt
    scala_3
    scalafmt
    selene
    tinymist
    typstyle
  ];

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
}
