{ inputs, ... }:
{
  perSystem =
    {
      inputs',
      system,
      lib,
      pkgs,
      ...
    }:
    with pkgs;
    {
      formatter = nixfmt-rfc-style;

      # Allow home-manager to be executed by running `nix run .#home-manager`
      apps.home-manager = {
        type = "app";
        program = "${inputs'.home-manager.packages.home-manager}/bin/home-manager";
      };

      checks = {
        pre-commit-check = inputs.git-hooks.lib.${system}.run {
          src = ../.;
          hooks = {
            statix.enable = true;
            nixfmt-rfc-style.enable = true;
            yamllint.enable = true;
          };
        };

        lint-playbooks = runCommand "lint-playbooks" { nativeBuildInputs = [ yamllint ]; } ''
          ${lib.getBin ansible-lint}/bin/ansible-lint
          touch "$out"
        '';
      };

      devShells.default = mkShellNoCC {
        inherit (inputs.self.checks.${system}.pre-commit-check) shellHook;

        name = "dotfiles-ansible";
        buildInputs = inputs.self.checks.${system}.pre-commit-check.enabledPackages;
        packages = [
          inputs'.disko.packages.disko
          inputs'.home-manager.packages.home-manager

          apk-tools
          arch-install-scripts
          ansible
          ansible-lint
        ];
      };
    };
}
